application_path = ENV['RAILS_ROOT']

environment ENV["RAILS_ENV"] || 'development'

directory application_path

workers Integer(ENV['WEB_CONCURRENCY'] || 2)
threads_count = Integer(ENV['RAILS_MAX_THREADS'] || 5)
threads threads_count, threads_count

preload_app!

pidfile "#{application_path}/tmp/pids/unicorn.pid"

state_path "#{application_path}/tmp/pids/puma.state"

stdout_redirect "#{application_path}/log/puma.stdout.log", "#{application_path}/log/puma.stderr.log"

port ENV['PORT'] || 3000

on_worker_boot do
  ActiveSupport.on_load(:active_record) do
    config = ActiveRecord::Base.configurations[Rails.env] ||
        Rails.application.config.database_configuration[Rails.env]
    config['pool'] = ENV['MAX_THREADS'] || 5
    ActiveRecord::Base.establish_connection(config)
  end
end